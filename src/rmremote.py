import os
import git, util

def main(ui, configs, opts):
    s = util.get_satchel(configs, opts.satchel)
    if s is None:
        ui.critical('Satchel %s not found' % opts.satchel)
        return -1

    # Sanity check the remote name.
    if opts.remote in ['master', 'local']:
        ui.critical('You cannot remove %s' % opts.remote)
        return -1
    r = None
    for remote in s.getchildren('remote'):
        if remote['name'] == opts.remote:
            r = remote
            break
    if r is None:
        ui.critical('Remote %s not found' % opts.remote)
        return -1

    if not opts.force:
        y = raw_input('Do you really want to remove this remote? [y/N]')
        if y != 'y':
            return -1

    try:
        os.chdir(s['path'])
    except:
        ui.exception('Failed to change to %s' % s['path'])
        return -1

    ret, _ = git.commit(ui)
    if ret != 0:
        ui.critical('Failed to save current repository state')
        return ret

    ret, _ = git.delete(ui, opts.remote)
    if ret != 0:
        ui.critical('Failed to delete branch')
        return ret

    s.remove('remote', lambda x: x['name'] == opts.remote)

    return 0
