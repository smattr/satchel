import itertools, os, re, subprocess

def run(ui, command, cwd=None):
    ui.info('Running \'%s\'...' % ' '.join(command))
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=cwd or os.curdir)
    output, _ = p.communicate()
    ui.info('%sProcess completed with return code %d' % (output, p.returncode))
    return p.returncode, output

def get_server(url):
    m = re.match(r'^ssh://(?:.*@)?([^@/]+)(?:/.*)?$', url)
    if m:
        return m.group(1)
    return None

def available(url):
    host = get_server(url)
    if not host:
        return False
    if run(['ping', '-c', '1', '-w', '5', host])[0] == 0:
        return True
    return False

def get_satchel(configs, name):
    s = None
    for n in itertools.chain.from_iterable([c.getchildren('satchel') for c in configs.values()]):
        if n['name'] == name:
            s = n
            break
    return s
