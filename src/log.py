import logging, sys

DEBUG = 1
INFO = 2
WARNING = 3
ERROR = 4
CRTIICAL = 5

DEFAULT = WARNING

def get_log(level):
    l = logging.DEBUG if level <= DEBUG else \
        logging.INFO if level == INFO else \
        logging.WARNING if level == WARNING else \
        legging.ERROR if level == ERROR else \
        logging.CRITICAL if level == CRITICAL else \
        logging.CRITICAL + 1
    log = logging.getLogger(sys.argv[0])
    log.setLevel(l)
    log.addHandler(logging.StreamHandler(sys.stderr))
    return log
