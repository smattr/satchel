import os
import config, git

def main(ui, configs, opts):
    if len(configs) != 1:
        ui.critical('You must use a single configuration file when creating a new satchel')
        return -1
    config = configs.values()[0]

    if reduce(lambda acc, x: acc or x['name'] == opts.satchel,
            config.getchildren('satchel'), False):
        ui.critical('Satchel %s already exists' % opts.satchel)
        return -1

    opts.directory = os.path.abspath(opts.directory)

    if os.path.exists(os.path.join(opts.directory, '.git')):
        ui.critical('Already existing git repository')
        return -1

    ret, _ = git.init(ui, opts.directory)
    if ret != 0:
        ui.critical('Failed to create repository')
        return ret

    try:
        os.chdir(opts.directory)
    except:
        ui.exception('Failed to change to %s' % opts.directory)
        return -1

    ret, _ = git.commit(ui, 'init %s (master)' % opts.satchel)
    if ret != 0:
        ui.critical('Failed initial commit on master')
        return ret

    ret, _ = git.checkout(ui, 'local', orphan=True)
    if ret != 0:
        ui.critical('Failed to checkout local')
        return ret

    ret, _ = git.add(ui)
    if ret != 0:
        ui.critical('Failed to add all existing files')
        return ret

    ret, _ = git.commit(ui, 'init %s (local)' % opts.satchel)
    if ret != 0:
        ui.critical('Failed to make initial commit')
        return ret

    n = config.add('satchel', name=opts.satchel, path=opts.directory)
    
    return ret
