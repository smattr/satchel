import xml.dom.minidom as minidom

ELEMENT_NODE = minidom.Node.ELEMENT_NODE

INDENT = '    '

class Node(object):
    def __init__(self, node=None):
        self.name = None
        self.attributes = {}
        self.dirty = False
        self.children = {}
        if node is not None:
            self.name = node.tagName
            attrs = node.attributes
            for i in range(attrs.length):
                self.attributes[attrs.item(i).name] = attrs.item(i).value
            for c in filter(lambda n: n.nodeType == ELEMENT_NODE, node.childNodes):
                
                if c.tagName not in self.children:
                    self.children[c.tagName] = []
                self.children[c.tagName].append(Node(c))

    def __contains__(self, key):
        return key in self.attributes

    def add(self, key, **kwargs):
        self.dirty = True
        n = Node()
        n.name = key
        for k in kwargs:
            n[k] = kwargs[k]
        if key not in self.children:
            self.children[key] = []
        self.children[key].append(n)

    def remove(self, key, guard):
        self.dirty = True
        self.children[key] = filter(lambda x: not guard(x), self.children[key])

    def getchildren(self, key):
        return self.children.get(key, [])

    def __getitem__(self, key):
        return self.attributes[key]

    def __setitem__(self, key, value):
        self.attributes[key] = value
        self.dirty = True

    def isdirty(self):
        return self.dirty or \
            reduce(lambda acc, x: acc or \
                reduce(lambda acca, y: acca or y.isdirty(), self.children[x], False), \
                    self.children, False)

    def writeto(self, f, indent=''):
        print >>f, '%(indent)s<%(name)s %(attributes)s>' % {
            'indent':indent,
            'name':self.name,
            'attributes':' '.join(map(lambda x: '%s="%s"' % (x[0], x[1]), self.attributes.items())),
        }
        for c in self.children:
            for ch in self.children[c]:
                ch.writeto(f, '%s%s' % (INDENT, indent))
        print >>f, '%(indent)s</%(name)s>' % {
            'indent':indent,
            'name':self.name,
        }

class Config(object):
    def __init__(self, fd):
        document = minidom.parse(fd)
        self.root = Node(document.documentElement)

    def __contains__(self, key):
        return key in self.root

    def __getitem__(self, key):
        return self.root[key]

    def add(self, key, **kwargs):
        self.root.add(key, **kwargs)

    def isdirty(self):
        return self.root.isdirty()

    def save(self, path):
        with open(path, 'w') as f:
            self.root.writeto(f)

    def getchildren(self, key):
        return self.root.getchildren(key)
