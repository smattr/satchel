#!/usr/bin/env python

import argparse, os, sys
import config, log
import init, mkremote, rmremote

COMMANDS = {
    'init':init.main,
    'mkremote':mkremote.main,
    'rmremote':rmremote.main,
    'sync':None,
}

USAGE = '''usage: %(prog)s command ...
Valid commands are %(commands)s.
Run '%(prog)s command --help' for information about specific commands''' % {
    'prog':sys.argv[0],
    'commands':', '.join(COMMANDS),
}

VERSION = '1.0'
UNDERSTOOD = [VERSION]

# Return codes. Could be adjusted later.
OK = 0
ERR_INCOMPATIBLE_CONFIG = -1
ERR_IO = -1

DEFAULT_CONFIG_CONTENTS = '<satchels version="%s"></satchels>' % VERSION
def default_config():
    return os.path.expanduser('~/.satchel.xml')

def parse_arguments():
    if len(sys.argv) < 2:
        print >>sys.stderr, USAGE
        sys.exit(-1)
    elif sys.argv[1] in ['--help', '-?', '-h']:
        print >>sys.stderr, USAGE
        sys.exit(0)
    elif sys.argv[1] == '--version':
        print 'satchel version %s' % VERSION
        sys.exit(0)
    elif sys.argv[1] not in COMMANDS:
        print >>sys.stderr, USAGE
        sys.exit(-1)

    command = sys.argv[1]
    assert command in COMMANDS

    # Setup command-following arguments.
    parser = argparse.ArgumentParser('%s %s' % (sys.argv[0], command))
    parser.add_argument('satchel',
        help='Name of the satchel to operate on')
    parser.add_argument('-F', '--file',
        help='Configuration file to read',
        action='append')
    parser.add_argument('-v', '--verbose',
        help='Increase verbosity level',
        action='count', default=0)
    parser.add_argument('-q', '--quiet',
        help='Decrease verbosity level',
        action='count', default=0)
    if command == 'init':
        parser.add_argument('directory',
            help='Directory to create in',
            nargs='?', default=os.curdir)
    elif command == 'mkremote':
        parser.add_argument('remote',
            help='Name of remote')
        parser.add_argument('path',
            help='Path to remote')
    elif command == 'rmremote':
        parser.add_argument('remote',
            help='Name of remote')
        parser.add_argument('-f', '--force',
            help='Don\'t ask for confirmation',
            action='store_true', default=False)
    elif command == 'sync':
        pass
    args = parser.parse_args(sys.argv[2:])
    args.command = command
    if not args.file:
        args.file = [default_config()]
    return args

def main():
    opts = parse_arguments()
    print opts

    ui = log.get_log(log.DEFAULT - opts.verbose + opts.quiet)

    # Parse configuration file(s).
    configs = {}
    for c in opts.file:
        ui.debug('Opening configuration %s' % c)
        try:
            if c == default_config() and not os.path.exists(c):
                ui.info('Creating a default configuration')
                with open(c, 'w') as f:
                    f.write(DEFAULT_CONFIG_CONTENTS)
            with open(c, 'r') as f:
                configs[c] = config.Config(f)
            if 'version' not in configs[c] or configs[c]['version'] not in UNDERSTOOD:
                ui.critical('Configuration %s is not compatible with this version of satchel.' % c)
                return ERR_INCOMPATIBLE_CONFIG
        except:
            ui.exception('Failed to open %s' % c)
            return ERR_IO

    ret = COMMANDS[opts.command](ui, configs, opts)

    # Save config changes if necessary.
    if ret == 0:
        for c in configs:
            if configs[c].isdirty():
                configs[c].save(c)

    return ret

if __name__ == '__main__':
    sys.exit(main())
