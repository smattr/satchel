import os
import util

def add(ui):
    cmd = ['git', 'add', '--all', '.']
    return util.run(ui, cmd)

def checkout(ui, branch, orphan=False, force=False):
    cmd = ['git', 'checkout']
    if orphan:
        cmd.append('--orphan')
    cmd.append(branch)
    if force:
        cmd.append('-f')
    return util.run(ui, cmd)

def commit(ui, message=None):
    cmd = ['git', 'commit', '--allow-empty', '-m', message or 'Automatic commit']
    return util.run(ui, cmd)

def delete(ui, branch):
    cmd = ['git', 'branch', '-D', branch]
    return util.run(ui, cmd)

def init(ui, path=os.curdir):
    cmd = ['git', 'init', path]
    return util.run(ui, cmd)

def rm(ui):
    cmd = ['git', 'rm', '-r', '--cached', '.']
    return util.run(ui, cmd)
