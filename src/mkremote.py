import itertools, os
import config, git, util

def main(ui, configs, opts):
    s = util.get_satchel(configs, opts.satchel)
    if s is None:
        ui.critical('Satchel %s not found' % opts.satchel)
        return -1

    # Sanity check the remote name.
    if opts.remote in ['master', 'local']:
        ui.critical('You cannot name a remote %s' % opts.remote)
        return -1
    if reduce(lambda acc, x: acc or x['name'] == opts.remote,
            s.getchildren('remote'), False):
        ui.critical('Remote %s already exists' % opts.remote)
        return -1

    try:
        os.chdir(s['path'])
    except:
        ui.exception('Failed to change to %s' % s['path'])
        return -1

    ret, _ = git.commit(ui)
    if ret != 0:
        ui.critical('Failed to save current repository state')
        return ret

    # We want to create an orphaned commit on a new branch.

    # Create the branch.
    ret, _ = git.checkout(ui, opts.remote, orphan=True)
    if ret != 0:
        ui.critical('Failed to create new branch')
        return ret

    # Reset automatically staged files.
    ret, _ = git.rm(ui)
    if ret != 0:
        ui.critical('Failed to unstage files')
        return ret

    # Create a new root.
    ret, _ = git.commit(ui, 'mkremote %s' % opts.remote)
    if ret != 0:
        ui.critical('Failed to create new commit')
        return ret

    # Switch back to local.
    ret, _ = git.checkout(ui, 'local', force=True)
    if ret != 0:
        ui.critical('Failed to switch back to local')
        return ret

    s.add('remote', name=opts.remote, path=opts.path)

    return 0
